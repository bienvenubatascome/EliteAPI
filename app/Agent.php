<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{

    protected $fillable= [
        'phone',
        'region_id',
        'code',
        'password',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function plannings()
    {
        return $this->hasMany('App\Planning');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany

    public function users()
    {
        return $this->morphMany('App\User', 'userable');
    }*/
}
