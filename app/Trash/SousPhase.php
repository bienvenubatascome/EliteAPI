<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SousPhase extends Model
{
    protected $fillable = [
        'nom',
    ];
}
