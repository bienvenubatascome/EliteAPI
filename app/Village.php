<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{

    protected $fillable = ['nom', 'canton_id'];


    public function canton()
    {
        return $this->belongsTo('App\Canton');
    }
}
