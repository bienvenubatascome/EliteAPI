<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->string('path');

            $table->integer('arbre_id')->unsigned()->index();
            $table->foreign('arbre_id')->references('id')->on('arbres');

            $table->integer('phase_id')->unsigned()->index();
            $table->foreign('phase_id')->references('id')->on('phases');

            $table->integer('sous_phase_id')->unsigned()->index();
            $table->foreign('sous_phase_id')->references('id')->on('sous_phases');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
