<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('cycles', 'CyclesController');
Route::resource('regions', 'RegionsController');
Route::resource('prefectures', 'PrefecturesController');
Route::resource('cantons', 'CantonsController');
Route::resource('villages', 'VillagesController');
Route::resource('agents', 'AgentsController');
Route::resource('producteurs', 'ProducteursController');
Route::resource('attributs', 'AttributsController');
Route::resource('visites', 'VisitesController');
Route::resource('arbres', 'ArbresController');
Route::resource('medias', 'MediasController');
